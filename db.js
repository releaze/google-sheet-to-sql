const mysql = require("mysql2/promise");
const config = require("config").get("sql");

const connection = async () => {
  return mysql.createConnection({
    host: config.host,
    user: config.user,
    database: config.database,
    password: config.password,
    port: config.port,
  });
};

const getRows = async () => {
  const conn = await connection();

  const [rows, fileds] = await conn
    .execute(`SELECT * FROM ${config.collection}`)
    .catch((err) => {
      console.log(err);
    });

  await conn.end();
  return rows;
};

const postRows = async (values) => {
  const conn = await connection();
  const sql = `INSERT INTO ${config.collection} (id, participantFK, type, name, source, language) VALUES ?`;

  await conn
    .query(sql, [values])
    .then((res) => {
      console.log('--->', `${res[0].affectedRows} records inserted to db`);
    })
    .catch((err) => {
      console.log(err);
    });

  await conn.end();
};

module.exports = {
  postRows,
  getRows,
};
