const GoogleSpreadsheet = require("google-spreadsheet");
const { promisify } = require("util");
const config = require("config").get("google");
const { getRows, postRows } = require("./db");
const { uniqBy } = require("lodash");
const { nanoid } = require("nanoid");

const run = async () => {
  const doc = new GoogleSpreadsheet(config.documentId);
  await promisify(doc.useServiceAccountAuth)(config);
  const info = await promisify(doc.getInfo)();
  const googleSheet = info.worksheets[0];
  const googleRows = await promisify(googleSheet.getRows)({ offset: 0 });
  const dbGetRows = await getRows();

  const thead = Object.keys(googleRows[0]).filter(
    (key) =>
      ![
        "_xml",
        "id",
        "app:edited",
        "_links",
        "save",
        "del",
        "participantfk",
      ].includes(key)
  );

  const rowsWithId = uniqBy(googleRows, "participantfk").reduce(
    (accum, curr) => {
      if (!curr.participantfk) return accum;

      const rowsById = thead.reduce((acc, name) => {
        const isAlternative = name.includes("alternative");

        //Filtered duplicate rows by participantFK and type from db
        if (curr[name]) {
          const notUniqueRow = dbGetRows.find(r => r.participantFK === +curr.participantfk && r.type === name);
          const notUniqueAlternativeRow = dbGetRows.find(r => r.type === 'alternative' && r.name === curr[name]);

          if (notUniqueRow || notUniqueAlternativeRow) return acc;
        }

        return curr[name]
          ? [
              ...acc,
              {
                id: nanoid(24),
                participantFK: curr.participantfk,
                type: !isAlternative ? name : "alternative",
                name: curr[name],
                source: "manual",
                language: "en",
              },
            ]
          : acc;
      }, []);

      return [...accum, ...rowsById];
    },
    []
  );

  const filteredAlternative = rowsWithId.reduce((accum, curr) => {
    const isAlternative = curr.type.includes("alternative");
    const isSame = accum.find((o) => o.participantFK === curr.participantFK && o.type.includes("alternative") && curr.type.includes("alternative") && o.name === curr.name);

    return isAlternative && isSame ? accum : [...accum, curr];
  }, []);

  const values = filteredAlternative.reduce((accum, curr) => {
    return [...accum, Object.values(curr)];
  }, []);

  if (values.length) {
    await postRows(values).catch(console.error);
  } else {
      console.log('--->', `Don't have records to insert`);
  }
};

run().catch(console.error);
